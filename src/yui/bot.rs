use std::collections::{HashSet};
use std::sync::Arc;
use serenity::client::Client;
use serenity::framework::standard::{help_commands, StandardFramework};
use serenity::framework::standard::DispatchError::OnlyForOwners;
use serenity::model::prelude::{User, CurrentUser};
use serenity;
use yui::config::Config;
use yui::commands::*;
use yui::events::Handler;
use yui::data::{Uptime, VoiceManager};

pub fn user() -> CurrentUser {
    serenity::http::get_current_user().unwrap()
}

pub fn owner() -> User {
    serenity::http::get_current_application_info()
        .unwrap()
        .owner
}

pub fn init() {
    let config = Config::load().expect("Failed to load config");

    let mut client = Client::new(&config.token, Handler).expect("Failed to create bot!");

    let mut owners = HashSet::new();
    owners.insert(owner().id);

    client.with_framework(
        StandardFramework::new()
            .configure(|c| c
                .prefix(&config.prefix)
                .allow_whitespace(config.prefix_whitespace)
                .case_insensitivity(config.command_case_insensitive)
                .on_mention(true)
                .ignore_bots(true)
                .owners(owners))
            .on_dispatch_error(|_ctx, msg, err| {
                match err {
                    OnlyForOwners => warn!("{} ({}) tried using `{}`", msg.author.tag(), msg.author.id, msg.content),
                    _ => ()
                }
            })
            .help(help_commands::with_embeds)
            .cmd("did_it_work", yes)
            .cmd("info", info)
            .cmd("invite", invite)
            .cmd("avatar", get_avatar)
            .group("Owner", |g| g
                .owners_only(true)
                .help_available(false)
                .command("shell", |c| c
                    .cmd(shell)
                    .known_as("sh"))
                .command("node", |c| c
                    .cmd(node)
                    .known_as("js"))
                .command("ruby", |c| c
                    .cmd(ruby)
                    .known_as("rb"))
                .command("python", |c| c
                    .cmd(python)
                    .known_as("py"))
                .command("setgame", |c| c
                    .cmd(set_game)
                    .owners_only(true))
            )
            .group("Music", |g| g
                .cmd("summon", summon)
            )
    );

    {
        let mut data = client.data.lock();
        data.insert::<Uptime>(Uptime::new());
        data.insert::<Config>(config);
        data.insert::<VoiceManager>(Arc::clone(&client.voice_manager));
    }

    info!("Starting up...");

    if let Err(why) = client.start_autosharded() {
        error!("Failed to start client: {:?}", why);
    }
}
