use std::fmt;
use std::sync::Arc;
use typemap::Key;
use chrono::prelude::*;
use chrono;
use serenity::client::bridge::voice::ClientVoiceManager;
use serenity::prelude::Mutex;

#[derive(Clone)]
pub struct Uptime {
    pub starttime: DateTime<Utc>,
}

impl Uptime {
    pub fn new() -> Uptime {
        Self { starttime: Utc::now() }
    }

    fn uptime(&self) -> chrono::Duration {
        let now = Utc::now();
        now.signed_duration_since(self.starttime)
    }
}

impl fmt::Display for Uptime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let time = self.uptime();
        let hours = time.num_hours();
        let minutes = time.num_minutes() % 61;
        let seconds = time.num_seconds() % 61;
        write!(f, "{:02}:{:02}:{:02}", hours, minutes, seconds)
    }
}

impl Key for Uptime {
    type Value = Uptime;
}

pub struct VoiceManager;

impl Key for VoiceManager {
    type Value = Arc<Mutex<ClientVoiceManager>>;
}
