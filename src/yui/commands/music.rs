// use serenity::voice;
use yui::data::VoiceManager;

command!(summon(context, message) {
    let voice_state =
        match message.guild() {
            Some(guild) => {
                guild.read().voice_states.get(&message.author.id).cloned()
            },
            None => None
        };

    let mut voice_manager = {
        let data = context.data.lock();
        data.get::<VoiceManager>().cloned().unwrap()
    };

    if let (&Some(ref vs), Some(gid)) = (&voice_state, message.guild_id()) {
        let mut manager = voice_manager.lock();
        if let Some(cid) = vs.channel_id {
            manager.join(gid, cid);
        }
    }

    info!("\n{:#?}", &voice_state);
});
