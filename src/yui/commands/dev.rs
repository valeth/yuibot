use yui::utils;

command!(shell(_context, message, args) {
    if let Ok(reply) = utils::run_command("bash", vec!["-c", args.full()]) {
        let _ = message.channel_id.say(reply);
    };
});

command!(node(_context, message, args) {
    let script = format!("console.log(function() {{ return ({}); }}())", args.full());
    if let Ok(reply) = utils::run_command("node", vec!["-e", &script]) {
        let _ = message.channel_id.say(reply);
    };
});

command!(ruby(_context, message, args) {
    let script = format!("puts(-> do {} end.call())", args.full());
    if let Ok(reply) = utils::run_command("ruby", vec!["-e", &script]) {
        let _ = message.channel_id.say(reply);
    };
});

command!(python(_context, message, args) {
    let script = format!("print((lambda: {})())", args.full());
    if let Ok(reply) = utils::run_command("python", vec!["-c", &script]) {
        let _ = message.channel_id.say(reply);
    };
});

command!(set_game(context, _message, args) {
    let game = args.full().to_owned();
    utils::set_game(&context, game);
});
