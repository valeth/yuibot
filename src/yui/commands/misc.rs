use serenity::utils::MessageBuilder;
use yui::bot::user as bot_user;
use yui::bot::owner as bot_owner;
use yui::data::Uptime;

command!(yes(_context, message) {
    let _ = message.reply("はい");
});

command!(info(context, message) {
    let startup = {
        let data = context.data.lock();
        let uptime = data.get::<Uptime>().unwrap();
        uptime.clone()
    };


    let info_message = format!("
        **Bot info:**\n\
        ```yaml\n\
        User:     {}\n\
        Owner:    {}\n\
        Famework: Serenity v{}\n\
        Uptime:   {}\n\
        ```",
        bot_user().tag(),
        bot_owner().tag(),
        "0.5.1", startup);

    let reply = MessageBuilder::new()
        .push(info_message)
        .build();

    let _ = message.channel_id.say(reply);
});

command!(invite(_context, message) {
    let reply = MessageBuilder::new()
        .push("<https://discordapp.com/oauth2/authorize?client_id=")
        .push(bot_user().id)
        .push("&scope=bot>")
        .build();

    let _ = message.channel_id.say(reply);
});

command!(get_avatar(_context, message) {
    let mentioned = match message.mentions.first() {
        Some(mention) => mention,
        None => &message.author
    };

    if let Some(url) = mentioned.avatar_url() {
        let _ = message.channel_id.send_message(|m| m
            .embed(|e| e
                .title(format!("User avatar of {}", mentioned.tag()))
                .url(&url)
                .image(&url)
            )
        );
    }
});
