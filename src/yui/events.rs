use serenity::prelude::{EventHandler, Context};
use serenity::model::prelude::{Ready, Member, GuildId};
use yui::bot::user as bot_user;
use yui::config::Config;
use yui::utils;

pub struct Handler;

impl EventHandler for Handler {
    fn ready(&self, ctx: Context, _ready: Ready) {
        let config = {
            let data = ctx.data.lock();
            let config = data.get::<Config>().unwrap();
            config.clone()
        };

        info!(
            "Logged in as {} ({})",
            bot_user().tag(),
            bot_user().id
        );
        info!("Invite with: https://discordapp.com/oauth2/authorize?client_id={}&scope=bot", bot_user().id);
        utils::set_game(&ctx, config.playing);
    }

    fn guild_member_addition(&self, _ctx: Context, gid: GuildId, mem: Member) {
        let guild = gid.get().unwrap().name;
        let user = mem.user.read().tag();
        info!("User {} joined {}", user, guild);
    }
}
