use std::string::ToString;
use std::io::Result;
use std::process::{Command, Output};
use serenity::prelude::Context;
use serenity::model::prelude::Game;

pub fn set_game(ctx: &Context, game: String) {
    if game.is_empty() {
        info!("Resetting presence...");
        ctx.reset_presence();
    } else {
        info!(r#"Setting game to "{}""#, &game);
        ctx.set_game(Game::playing(&game));
    }
}

fn codeblock(output: Output, cmd: String) -> String {
    if output.status.success() {
        format!("```\n{}\n```", String::from_utf8_lossy(&output.stdout))
    } else {
        let err = String::from_utf8_lossy(&output.stderr);
        error!("cmd: {} | msg: {}", cmd, &err);
        format!("Error:```\n{}\n```", &err)
    }
}

pub fn run_command<A: ToString>(cmd: A, args: Vec<A>) -> Result<String> {
    let real_args: Vec<String> = args.iter().map(|x| x.to_string()).collect();
    let real_cmd = cmd.to_string();

    info!("cmd: {:?} | args: {:?}", real_cmd, real_args);

    let output = Command::new(&real_cmd).args(real_args).output()?;
    Ok(codeblock(output, real_cmd))
}
