use std::fs::File;
use std::path::Path;
use std::process::exit;
use std::io::Result;
use typemap::Key;
use serde_yaml;

fn default_prefix() -> String {
    "?".to_string()
}

#[derive(Debug, Default, PartialEq, Clone, Serialize, Deserialize)]
#[serde(default)]
pub struct Config {
    #[serde(default = "default_prefix")]
    pub prefix: String,
    pub token: String,
    pub playing: String,
    pub prefix_whitespace: bool,
    pub command_case_insensitive: bool,
}

impl Config {
    pub fn load() -> Result<Config> {
        let config_file = Path::new("config/discord.yml");

        if !config_file.exists() {
            println!("Config file missing");
            exit(1);
        }

        let file = File::open(&config_file)?;
        let config: Config = serde_yaml::from_reader(&file)
            .expect("Failed to parse config.");

        Ok(config)
    }
}

impl Key for Config {
    type Value = Config;
}

