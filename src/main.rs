#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serenity;
#[macro_use]
extern crate log;

extern crate simplelog;
extern crate serde_yaml;
extern crate chrono;
extern crate typemap;

mod yui;

use simplelog::{Config, TermLogger, LogLevelFilter};

fn main() {
    match TermLogger::init(LogLevelFilter::Info, Config::default()) {
        Err(why) => println!("Failed to initialize logger: {}", why),
        Ok(_) => ()
    };

    yui::init();
}
