module Overcommit::Hook::PrePush
  class CargoTest < Base
    def run
      result = execute(command)
      return :pass if result.success?
      output = result.stdout + result.stderr
      [:fail, output]
    end
  end
end
